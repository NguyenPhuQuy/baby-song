"use strict"; //Jquery.inview.min.js

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (a) {
  "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? module.exports = a(require("jquery")) : a(jQuery);
}(function (a) {
  function i() {
    var b,
        c,
        d = {
      height: f.innerHeight,
      width: f.innerWidth
    };
    return d.height || (b = e.compatMode, (b || !a.support.boxModel) && (c = "CSS1Compat" === b ? g : e.body, d = {
      height: c.clientHeight,
      width: c.clientWidth
    })), d;
  }

  function j() {
    return {
      top: f.pageYOffset || g.scrollTop || e.body.scrollTop,
      left: f.pageXOffset || g.scrollLeft || e.body.scrollLeft
    };
  }

  function k() {
    if (b.length) {
      var e = 0,
          f = a.map(b, function (a) {
        var b = a.data.selector,
            c = a.$element;
        return b ? c.find(b) : c;
      });

      for (c = c || i(), d = d || j(); e < b.length; e++) {
        if (a.contains(g, f[e][0])) {
          var h = a(f[e]),
              k = {
            height: h[0].offsetHeight,
            width: h[0].offsetWidth
          },
              l = h.offset(),
              m = h.data("inview");
          if (!d || !c) return;
          l.top + k.height > d.top && l.top < d.top + c.height && l.left + k.width > d.left && l.left < d.left + c.width ? m || h.data("inview", !0).trigger("inview", [!0]) : m && h.data("inview", !1).trigger("inview", [!1]);
        }
      }
    }
  }

  var c,
      d,
      h,
      b = [],
      e = document,
      f = window,
      g = e.documentElement;
  a.event.special.inview = {
    add: function add(c) {
      b.push({
        data: c,
        $element: a(this),
        element: this
      }), !h && b.length && (h = setInterval(k, 250));
    },
    remove: function remove(a) {
      for (var c = 0; c < b.length; c++) {
        var d = b[c];

        if (d.element === this && d.data.guid === a.guid) {
          b.splice(c, 1);
          break;
        }
      }

      b.length || (clearInterval(h), h = null);
    }
  }, a(f).on("scroll resize scrollstop", function () {
    c = d = null;
  }), !g.addEventListener && g.attachEvent && g.attachEvent("onfocusin", function () {
    d = null;
  });
});

(function () {
  $(document).ready(function () {
    /*===================================================
    Add css slide
    ===================================================*/
    $('.mainSlide-item').css({
      "background-repeat": "no-repeat",
      "background-size": "cover",
      "background-position": "100% center"
    });
    /*===================================================
    Slick slide
    ===================================================*/

    $('.mainSlide-list').slick({
      infinite: true,
      autoplay: true,
      fade: true,
      autoplaySpeed: 3000,
      speed: 1500,
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false
    });
    /*===================================================
    Menu nav sp
    ===================================================*/

    $('.nav-btn').on('click', function () {
      $('.nav-sp').toggleClass('is-active');
      $('.wrapper').toggleClass('is-active');
      $('.nav-btn').toggleClass('is-active');
    });
    /*===================================================
    Scroll to Top
    ===================================================*/

    $(document).ready(function () {
      $('.pagetop-svg').click(function () {
        $('html,body').animate({
          scrollTop: 0
        }, 1000);
      });
      $(".pagetop-svg").hide();
      $(window).scroll(function () {
        if ($("html,body").scrollTop() > 600) {
          $(".pagetop-svg").fadeIn();
        } else {
          $(".pagetop-svg").fadeOut();
        }
      });
    });
    /*===================================================
    Style Background navigation when scroll
    ===================================================*/

    var windowNav = $(window);
    var locationNav = $('.mainSlide-catch').offset().top;
    $(windowNav).on('load scroll', function () {
      if (windowNav.scrollTop() >= locationNav) {
        $('.header').addClass('active-bg');
      } else {
        $('.header').removeClass('active-bg');
      }
    });
    /*===================================================
    Fade Element
    ===================================================*/

    $('.zoom').on('inview', function (event, isInview, visiblePartX, visiblePartY) {
      if (isInview) {
        $(this).stop().addClass('zoomIn');
      }
    });
    $('.fadeLeft').on('inview', function (event, isInview, visiblePartX, visiblePartY) {
      if (isInview) {
        $(this).stop().addClass('fadeInLeft');
      }
    });
    $('.fadeRight').on('inview', function (event, isInview, visiblePartX, visiblePartY) {
      if (isInview) {
        $(this).stop().addClass('fadeInRight');
      }
    });
    $('.fadeUp').on('inview', function (event, isInview, visiblePartX, visiblePartY) {
      if (isInview) {
        $(this).stop().addClass('fadeInUp');
      }
    });
  });
})();